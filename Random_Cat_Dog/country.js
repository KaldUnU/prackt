fetch('https://restcountries.eu/rest/v2/regionalbloc/eu')
    .then((response) =>{
    return response.json();
    })
    .then((resp) => {
    var ListOfCapitals=document.createElement('ul');
    ListOfCapitals.classList.add("capital");
    resp.forEach(function(item) {
    var listViewItem=document.createElement('li');
    listViewItem.appendChild(document.createTextNode(item.capital));
    ListOfCapitals.appendChild(listViewItem);
    });
    document.getElementsByClassName("col-5")[0].getElementsByClassName("capital")[0].appendChild(ListOfCapitals)
    });